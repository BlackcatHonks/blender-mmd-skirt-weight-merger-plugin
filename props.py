import bpy

class SkirtWeightMergerProps(bpy.types.PropertyGroup):
    group_pattern = bpy.props.StringProperty(
        name = "Skirt Group Pattern",
        description = "A regular expression describing a pattern the MMD armature uses for skirt bones",
        default = "Skirt"
    )
    exclusion_pattern = bpy.props.StringProperty(
        name = "Exclusion Skirt Group Pattern",
        description = "A regular expression describing a pattern the MMD armature must exclude for skirt bones",
        default = "Parent"
    )
    custom_parent_name = bpy.props.StringProperty(
        name = "Custom Parent Name (Leave blank to find automatically)",
        default = "",
    )
    left_leg_name = bpy.props.StringProperty(
        name = "Left Leg Bone Name",
        default = "Left leg",
    )
    right_leg_name = bpy.props.StringProperty(
        name = "Right Leg Bone Name",
        default = "Right leg",
    )
    left_skirt_name = bpy.props.StringProperty(
        name = "New Left Skirt Name",
        default = "Left skirt",
    )
    right_skirt_name = bpy.props.StringProperty(
        name = "New Right Skirt Name",
        default = "Right skirt",
    )
    starting_row = bpy.props.IntProperty(
        name = "Starting row",
        description = "Starting row to keep, starting from 0 at the top",
        default = 1
    )
    ending_row = bpy.props.IntProperty(
        name = "Ending row",
        description = "Ending row to merge, -1 for all, starting from 0 at the top",
        default = -1
    )
    merge_above = bpy.props.BoolProperty(
        name = "Merge above",
        description = "Merge all rows above",
        default = True
    )
    center_x_deviation = bpy.props.FloatProperty(
        name = "Center X Deviation",
        description = "How far from an X position of 0 to still be considered a 'Center' bone.",
        default = 0.01
    )