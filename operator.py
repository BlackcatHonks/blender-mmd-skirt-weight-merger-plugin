import bpy
import re
import math 
from . import utils

class SkirtWeightMergerUpperDefault(bpy.types.Operator):
    bl_idname = "blackcat.swm_upper_default"
    bl_label = "Upper Defaults"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        props = context.scene.skirt_weight_merger_props
        props.left_leg_name = "Left leg"
        props.right_leg_name = "Right leg"
        props.left_skirt_name = "Left skirt"
        props.right_skirt_name = "Right skirt"
        props.starting_row = 1
        props.ending_row = -1
        props.merge_above = True

        return {'FINISHED'}

class SkirtWeightMergerLowerDefault(bpy.types.Operator):
    bl_idname = "blackcat.swm_lower_default"
    bl_label = "Lower Defaults"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        props = context.scene.skirt_weight_merger_props
        props.left_leg_name = "Left knee"
        props.right_leg_name = "Right knee"
        props.left_skirt_name = "Left skirt lower"
        props.right_skirt_name = "Right skirt lower"
        props.starting_row = 3
        props.ending_row = -1
        props.merge_above = False

        return {'FINISHED'}

class SkirtWeightMergerOperator(bpy.types.Operator):
    bl_label = "Skirt Weight Merger"
    bl_idname = "blackcat.skirt_weight_merger"
    bl_description = "Merges MMD skirts in a left and right bone, parented to the legs."
    bl_options = {"REGISTER", "UNDO"}
    debug = True

    def execute(self, context):
        utils.switch("OBJECT")
        obj = context.active_object
        while (obj and obj.type != "ARMATURE"):
            obj = obj.parent
            utils.set_active(obj)
        if (obj is None):
            obj = obj.data.objects["Armature"]
        if (obj is None or obj.type != "ARMATURE"):
            raise Exception("Armature not found in the Scene.")
        utils.switch("EDIT")
 
        props = context.scene.skirt_weight_merger_props
        parent = None
        bone_names = list(filter(lambda bone_name: re.search(".*" + props.group_pattern + ".*", bone_name) and not re.search(".*" + props.exclusion_pattern + ".*", bone_name), obj.data.edit_bones.keys()))

        if (len(bone_names) == 0):
            raise Exception("No bones were found matching the pattern " + props.group_pattern + ", cannot continue.")
        if (props.custom_parent_name):
            parent = obj.data.edit_bones[props.custom_parent_name]
        else:
            parents = []
            for bone_name in bone_names:
                bone = obj.data.edit_bones[bone_name]
                if (bone.parent and bone.parent in parents):
                    parent = bone.parent
                    break
                elif (bone.parent and bone.parent.name not in bone_names):
                    parents.append(bone.parent)
        if not parent:
            raise Exception("Parent not found, cannot continue.")
        if self.debug:
            utils.print_bones("PARENT BONE", [parent])
        parent_name = parent.name

        parent_center_x = parent.center.x
        bones = list(filter(lambda bone: re.search(".*" + props.group_pattern + ".*", bone.name) and not re.search(".*" + props.exclusion_pattern + ".*", bone_name), parent.children))
        bone_names = utils.bones_from_row(props.starting_row, bones)
        utils.merge_children(props.starting_row, props.ending_row, props.merge_above, obj, bones)
        utils.switch("EDIT")

        parent = obj.data.edit_bones[parent_name]
        bones = list(map(lambda bone_name: obj.data.edit_bones[bone_name], bone_names))
        center_bones = list(filter(lambda bone: math.fabs(bone.center.x - parent_center_x) < props.center_x_deviation, bones))
        center_bone_names = list(map(lambda bone: bone.name, center_bones))
        
        if self.debug:
            utils.print_bones("CENTER SKIRT BONES", center_bones)

        utils.duplicate_bones(obj, center_bone_names, bone_names)

        parent = obj.data.edit_bones[parent_name]
        bones = list(map(lambda bone_name: obj.data.edit_bones[bone_name], bone_names))
        left_bones = list(filter(lambda bone: round(bone.center.x, 4) > parent_center_x, bones))
        left_bones.sort(key=lambda bone: bone.center.x)
        left_bone_names = list(map(lambda bone: bone.name, left_bones))
        right_bones = list(filter(lambda bone: round(bone.center.x, 4) < parent_center_x, bones))
        right_bones.sort(key=lambda bone: bone.center.x, reverse=True)
        right_bone_names = list(map(lambda bone: bone.name, right_bones))

        if self.debug:
            utils.print_bones("LEFT SKIRT BONES", left_bones)
            utils.print_bones("RIGHT SKIRT BONES", right_bones)

        left_bone_name  = utils.merge_bone_group(obj, left_bone_names)
        right_bone_name = utils.merge_bone_group(obj, right_bone_names)

        utils.rename_bone(obj, left_bone_name, props.left_skirt_name)
        utils.rename_bone(obj, right_bone_name, props.right_skirt_name)
        
        obj.data.edit_bones[props.left_skirt_name].parent = obj.data.edit_bones[props.left_leg_name]
        obj.data.edit_bones[props.right_skirt_name].parent = obj.data.edit_bones[props.right_leg_name]

        return {'FINISHED'}