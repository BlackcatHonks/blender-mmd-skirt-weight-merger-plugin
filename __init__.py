# MIT License

# Copyright (c) 2019 BlackCat OfIllOmen

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the 'Software'), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Code author: BlackCat OfIllOmen
# Common functionality derived from: Cats Blender Plugin by GiveMeAllYourCats, Hotox

bl_info = {
    "name": "MMD Skirt Weight Merger",
    "category": "3D View",
    "description": "Merges MMD skirts in a left and right bone, parented to the legs.",
    "location": "View 3D > Tool Shelf > MMD Extensions",
    "version": (0, 0, 4),
    "blender": (2, 80, 0),
    "warning": ""
}

import bpy
from . import operator
from . import panel
from . import props
from . import utils

def register():
    bpy.utils.register_class(panel.SkirtWeightMergerPanel)
    bpy.utils.register_class(operator.SkirtWeightMergerOperator)
    bpy.utils.register_class(operator.SkirtWeightMergerUpperDefault)
    bpy.utils.register_class(operator.SkirtWeightMergerLowerDefault)
    bpy.utils.register_class(props.SkirtWeightMergerProps)
    bpy.types.Scene.skirt_weight_merger_props = bpy.props.PointerProperty(type=props.SkirtWeightMergerProps)

def unregister():
    bpy.utils.unregister_class(panel.SkirtWeightMergerPanel)
    bpy.utils.unregister_class(operator.SkirtWeightMergerOperator)
    bpy.utils.unregister_class(operator.SkirtWeightMergerUpperDefault)
    bpy.utils.unregister_class(operator.SkirtWeightMergerLowerDefault)
    bpy.utils.unregister_class(props.SkirtWeightMergerProps)
    del bpy.types.Scene.skirt_weight_merger_props

if __name__ == "__main__":
    register()
    