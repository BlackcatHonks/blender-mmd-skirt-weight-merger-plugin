import bpy

def version_2_79_or_older():
    return bpy.app.version < (2, 80)

def rename_bone(obj, bone_name, new_bone_name):
    obj.data.edit_bones[bone_name].name = new_bone_name
    meshes = get_meshes_objects(armature_name=obj.name)
    if (len(meshes) > 0):
        vertex_group = meshes[0].vertex_groups.get(bone_name)
        if (vertex_group):
            vertex_group.name = new_bone_name
        return new_bone_name

def bones_from_row(starting_row, bones):
    bone_names = []
    for bone in bones:
        depth = 0
        while (depth < starting_row and len(bone.children) > 0 and bone):
            if (len(bone.children)):
                bone = bone.children[0]
                depth = depth + 1
        if (bone):
            bone_names.append(bone.name)
    return bone_names

def duplicate_bones(obj, center_bone_names, bone_names):
    for center_bone_name in center_bone_names:
        duplicate_bone(obj, center_bone_name, bone_names)
    unselect_all()
    set_active(obj)
    switch("EDIT")

def duplicate_bone(obj, center_bone_name, bone_names):
    unselect_all()
    set_active(obj)
    switch("EDIT")

    bone = obj.data.edit_bones[center_bone_name]
    new_bone = obj.data.edit_bones.new(bone.name + "_CLONE")
    new_bone.head = bone.head
    new_bone.tail = bone.tail
    new_bone.matrix = bone.matrix
    new_bone.parent = bone.parent
    new_bone.head.x = -0.001
    new_bone.tail.x = -0.001
    bone.head.x = 0.001
    bone.tail.x = 0.001
    bone_names.append(new_bone.name)

    switch("OBJECT")
    meshes = get_meshes_objects(armature_name=obj.name)
    mesh = meshes[0]
    set_active(mesh)
    switch("OBJECT")
    mesh.vertex_groups.new(name=new_bone.name)
    mix_weights(mesh, bone.name, new_bone.name, delete_old_vg=False)

def mix_weights(mesh, vg_from, vg_to, delete_old_vg=True):
    mesh.active_shape_key_index = 0
    mod = mesh.modifiers.new("VertexWeightMix", 'VERTEX_WEIGHT_MIX')
    mod.vertex_group_a = vg_to
    mod.vertex_group_b = vg_from
    mod.mix_mode = 'ADD'
    mod.mix_set = 'B'
    if version_2_79_or_older():
        bpy.ops.object.modifier_apply(modifier=mod.name)
    else:
        bpy.ops.object.modifier_apply({"object": mesh}, modifier=mod.name)

    if delete_old_vg:
        mesh.vertex_groups.remove(mesh.vertex_groups.get(vg_from))
    mesh.active_shape_key_index = 0  # This line fixes a visual bug in 2.80 which causes random weights to be stuck after being merged

def merge_weights(armature, bone_to_parent_names):
    switch("OBJECT")
    # Merge the weights on the meshes
    for mesh in get_meshes_objects(armature_name=armature.name):
        set_active(mesh)
        for bone in sorted(bone_to_parent_names, reverse=True):
            parent = bone_to_parent_names[bone]
            if not mesh.vertex_groups.get(bone):
                continue
            if not mesh.vertex_groups.get(parent):
                mesh.vertex_groups.new(name=parent)
            mix_weights(mesh, bone, parent, delete_old_vg=False)
        for bone, parent in bone_to_parent_names.items():
            if mesh.vertex_groups.get(bone):
                mesh.vertex_groups.remove(mesh.vertex_groups.get(bone))

    # Select armature
    unselect_all()
    set_active(armature)
    switch("EDIT")

    # Delete merged bones
    for bone in bone_to_parent_names.keys():
        armature.data.edit_bones.remove(armature.data.edit_bones.get(bone))

def is_selected(obj):
    if version_2_79_or_older():
        return obj.select
    return obj.select_get()

def get_objects():
    return bpy.context.scene.objects if version_2_79_or_older() else bpy.context.view_layer.objects

def unselect_all():
    for obj in get_objects():
        select(obj, False)

def merge_from_parent(obj, bones):
    bone_to_parent_list = {}
    for bone in bones:
        bone_to_parent_list[bone.name] = bone.parent.name
    bpy.ops.armature.select_all(action='DESELECT')

def merge_children(starting_row, ending_row, merge_above, obj, bones):
    bones_list = get_children(starting_row, ending_row, merge_above, obj, bones)
    switch("EDIT")
    bone_to_parent_list = {}
    for bone in bones_list:
        bone_to_parent_list[bone.name] = bone.parent.name
    merge_weights(obj, bone_to_parent_list)
    bpy.ops.armature.select_all(action='DESELECT')

def get_children(starting_row, ending_row, merge_above, obj, bones):
    bones_list = list()
    bpy.ops.armature.select_all(action='DESELECT')
    for bone in bones:
        get_child_rows(starting_row, ending_row, 0, merge_above, obj, bone, bones_list)
    return bones_list
     
def get_child_rows(starting_row, ending_row, current_row, merge_above, obj, bone, bones_list):
    if (not bone):
        return
    for child_bone in bone.children:
        get_child_rows(starting_row, ending_row, current_row + 1, merge_above, obj, child_bone, bones_list)
    if ((current_row > starting_row or merge_above) and (current_row != starting_row) and (current_row <= ending_row or ending_row < 0)):
        bones_list.append(bone)

def merge_bone_group(obj, bone_names):
    switch("EDIT")
    bone_to_parent_names = {}
    active_bone = bone_names[-1]
    for bone_name in bone_names:
        if bone_name != active_bone:
            bone_to_parent_names[bone_name] = active_bone
    merge_weights(obj, bone_to_parent_names)
    bpy.ops.armature.select_all(action='DESELECT')
    return active_bone

def select_bone_group(obj, bone_names):
    switch("EDIT")
    bpy.ops.armature.select_all(action='DESELECT')
    for bone_name in bone_names:
        select_bone(obj, bone_name)

def select_bone(obj, bone_name):
    edit_bone = obj.data.edit_bones[bone_name]
    edit_bone.select = True
    obj.data.edit_bones.active = edit_bone

def print_bones(message, bones):
    print("\n\n--------" + message + "--------")
    for bone in bones:
        print(bone.name + " (" + str(round(bone.center.x, 2)) + ")")

def switch(new_mode, check_mode=True):
    if check_mode and get_active() and get_active().mode == new_mode:
        return
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode=new_mode, toggle=False)

def select(obj, sel=True):
    if sel:
        hide(obj, False)
    if version_2_79_or_older():
        obj.select = sel
    else:
        obj.select_set(sel)

def hide(obj, val=True):
    if version_2_79_or_older():
        obj.hide = val
    else:
        obj.hide_viewport = val

def set_active(obj, skip_sel=False):
    if not skip_sel:
        select(obj)
    if version_2_79_or_older():
        bpy.context.scene.objects.active = obj
    else:
        bpy.context.view_layer.objects.active = obj

def get_active():
    if version_2_79_or_older():
        return bpy.context.scene.objects.active
    return bpy.context.view_layer.objects.active

def get_meshes_objects(armature_name=None, mode=0, check=True):
    # Modes:
    # 0 = With armatures only
    # 1 = Top level only
    # 2 = All meshes
    # 3 = Selected only

    meshes = []
    for ob in get_objects():
        if ob.type == 'MESH':
            if mode == 0:
                if not armature_name:
                    armature_name = bpy.context.scene.armature
                if ob.parent:
                    if ob.parent.type == 'ARMATURE' and ob.parent.name == armature_name:
                        meshes.append(ob)
                    elif ob.parent.parent and ob.parent.parent.type == 'ARMATURE' and ob.parent.parent.name == armature_name:
                        meshes.append(ob)

            elif mode == 1:
                if not ob.parent:
                    meshes.append(ob)

            elif mode == 2:
                meshes.append(ob)

            elif mode == 3:
                if is_selected(ob):
                    meshes.append(ob)

    # Check for broken meshes and delete them
    if check:
        current_active = get_active()
        to_remove = []
        for mesh in meshes:
            selected = is_selected(mesh)
            # print(mesh.name, mesh.users)
            set_active(mesh)

            if not get_active():
                to_remove.append(mesh)

            if not selected:
                select(mesh, False)

        for mesh in to_remove:
            print('DELETED CORRUPTED MESH:', mesh.name, mesh.users)
            meshes.remove(mesh)
            delete(mesh)

        if current_active:
            set_active(current_active)

    return meshes
