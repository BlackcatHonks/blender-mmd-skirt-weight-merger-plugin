import bpy
from . import utils

class SkirtWeightMergerPanel(bpy.types.Panel):
    """Creates a Panel for merging skirt weights"""
    bl_label = "Skirt Weight Merger"
    bl_idname = "VIEW3D_PT_skirt_weight_merger"
    bl_category = 'MMD Extensions'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS' if utils.version_2_79_or_older() else 'UI'

    def draw(self, context):
        layout = self.layout
        props = context.scene.skirt_weight_merger_props

        row = layout.row()
        row.label(text="Skirt Weight Merger", icon='BONE_DATA')
        row = layout.row()
        row.operator("blackcat.skirt_weight_merger", text="Merge Weights")
        row = layout.row()
        row.prop(props, "group_pattern")
        row = layout.row()
        row.prop(props, "starting_row")
        row = layout.row()
        row.prop(props, "ending_row")
        row = layout.row()
        row.prop(props, "merge_above")
        row = layout.row()
        row.prop(props, "left_skirt_name")
        row = layout.row()
        row.prop(props, "right_skirt_name")
        row = layout.row()
        row.prop(props, "left_leg_name")
        row = layout.row()
        row.prop(props, "right_leg_name")
        row = layout.row()
        row.prop(props, "exclusion_pattern")
        row = layout.row()
        row.prop(props, "custom_parent_name")
        row = layout.row()
        row.prop(props, "center_x_deviation")
        row = layout.row()
        row.operator("blackcat.swm_upper_default", text="Upper Defaults")
        row.operator("blackcat.swm_lower_default", text="Lower Defaults")